

```shell
set JAVA_HOME=
set SAP_HOME=ddl
set APP_HOME=

set SAP_JCO_ASHOST=
set SAP_JCO_SYSNR=
set SAP_JCO_CLIENT=
set SAP_JCO_USER=
set SAP_JCO_PASSWD=
set IXTF_API_PORT=

set JAVA_OPTS=""
set DEFAULT_JVM_OPTS="--add-modules" "java.se" "--add-exports" "java.base/jdk.internal.ref=ALL-UNNAMED" "--add-opens" "java.base/java.lang=ALL-UNNAMED" "--add-opens" "java.base/java.lang.invoke=ALL-UNNAMED" "--add-opens" "java.base/java.nio=ALL-UNNAMED" "--add-opens" "java.base/sun.nio.ch=ALL-UNNAMED" "--add-opens" "java.management/sun.management=ALL-UNNAMED" "--add-opens" "jdk.management/com.sun.management.internal=ALL-UNNAMED" "-XX:+UseContainerSupport" "-Dio.netty.tryReflectionSetAccessible=true"

set JAVA_EXE=%JAVA_HOME%/bin/java.exe
set CLASSPATH=%SAP_HOME%\sapjco3.jar;%APP_HOME%\sap-service.jar
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% -cp "%CLASSPATH%" com.gitee.ixtf.sap.ServiceLauncherKt
```