import com.google.cloud.tools.jib.api.buildplan.ImageFormat
import java.time.OffsetDateTime
import org.gradle.internal.os.OperatingSystem

plugins {
  application
  alias(libs.plugins.jvm)
  alias(libs.plugins.spotless)
  alias(libs.plugins.jib)
  alias(libs.plugins.shadow)
}

group = "cn.com.medipath"

version = "1.0.0"

dependencies {
  api(platform(libs.bom))
  api("com.gitee.ixtf:vertx")
  api("io.vertx:vertx-auth-jwt")

  implementation(kotlin("reflect"))

  testImplementation(platform(libs.junit.bom))
  testImplementation("org.junit.jupiter:junit-jupiter")
  testImplementation(libs.kotlin.test)
  testRuntimeOnly("org.junit.platform:junit-platform-launcher")

  val os = OperatingSystem.current()
  val sapJar =
      when {
        os.isLinux -> files("libs/sapjco30P_17-64-Linux/sapjco3.jar")
        else -> files("libs/sapjco30P_17-64-Windows/sapjco3.jar")
      }
  compileOnly(sapJar)
  testRuntimeOnly(sapJar)
}

application { mainClass.set("com.gitee.ixtf.sap.ServiceLauncherKt") }

jib {
  from { image = libs.versions.jib.image.jre.get() }
  to {
    image = "docker.medipath.com.cn/sap-service"
    tags = setOf("${project.version}", "latest")
  }
  container {
    mainClass = "com.gitee.ixtf.sap.ServiceLauncherKt"
    jvmFlags =
        listOf(
            "--add-modules",
            "java.se",
            "--add-exports",
            "java.base/jdk.internal.ref=ALL-UNNAMED",
            "--add-opens",
            "java.base/java.lang=ALL-UNNAMED",
            "--add-opens",
            "java.base/java.lang.invoke=ALL-UNNAMED",
            "--add-opens",
            "java.base/java.nio=ALL-UNNAMED",
            "--add-opens",
            "java.base/sun.nio.ch=ALL-UNNAMED",
            "--add-opens",
            "java.management/sun.management=ALL-UNNAMED",
            "--add-opens",
            "jdk.management/com.sun.management.internal=ALL-UNNAMED",
            "-XX:+UseContainerSupport",
            "-Dio.netty.tryReflectionSetAccessible=true",
        )
    format = ImageFormat.OCI
    creationTime = OffsetDateTime.now().toString()
  }
}

tasks {
  getByName<Test>("test") { useJUnitPlatform() }
  withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    mergeServiceFiles()
    archiveFileName.set("sap-service.jar")
  }
}

kotlin { jvmToolchain(21) }

spotless {
  java {
    target("**/java/**/*.java", "**/kotlin/**/*.java")
    targetExclude("**/generated/**", "**/generated_tests/**")
    googleJavaFormat()
    formatAnnotations()
    trimTrailingWhitespace()
    endWithNewline()
  }
  kotlin {
    target("**/java/**/*.kt", "**/kotlin/**/*.kt")
    targetExclude("**/generated/**", "**/generated_tests/**")
    ktfmt()
    trimTrailingWhitespace()
    endWithNewline()
  }
  kotlinGradle {
    target("*.gradle.kts", "additionalScripts/*.gradle.kts")
    ktfmt()
    trimTrailingWhitespace()
    endWithNewline()
  }
  format("styling") {
    target("**/resources/**/*.graphql", "**/resources/**/*.graphqls")
    prettier()
    trimTrailingWhitespace()
    endWithNewline()
  }
}
