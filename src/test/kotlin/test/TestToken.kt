package test

import com.gitee.ixtf.guice.Jguice
import com.gitee.ixtf.sap.ServiceModule
import io.vertx.core.Vertx
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.ext.auth.jwtOptionsOf

private val jwtAuth: JWTAuth by Jguice

fun main() {
  Jguice.init(ServiceModule(Vertx.vertx()))

  val jsonObject = jsonObjectOf("sub" to "admin")
  val jwtOptions = jwtOptionsOf(noTimestamp = true)
  println(jwtAuth.generateToken(jsonObject, jwtOptions))
}
