package debug

import com.gitee.ixtf.guice.Jguice
import com.gitee.ixtf.guice.kotlinx.get
import com.gitee.ixtf.sap.ServiceLauncher
import com.gitee.ixtf.sap.application.SapService
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.ext.web.common.WebEnvironment.SYSTEM_PROPERTY_NAME
import java.util.concurrent.TimeUnit

fun main(args: Array<String>) {
  System.setProperty(SYSTEM_PROPERTY_NAME, "dev")
  System.setProperty("ixtf.api.port", "30000")
  SapServiceDebug().dispatch(args)
}

class SapServiceDebug : ServiceLauncher() {
  override fun beforeStartingVertx(options: VertxOptions) {
    super.beforeStartingVertx(options)
    options.setMaxEventLoopExecuteTime(1).setMaxEventLoopExecuteTimeUnit(TimeUnit.DAYS)
  }

  override fun afterStartingVertx(vertx: Vertx) {
    super.afterStartingVertx(vertx)
    println(Jguice.get<SapService>())
  }
}
