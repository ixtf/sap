package com.gitee.ixtf.sap

import com.gitee.ixtf.core.kotlinx.ixtfEnvInt
import com.gitee.ixtf.core.kotlinx.ixtfEnvString
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.DEFAULT_CONCURRENCY

object Constant {
  val SAP_DESTINATION_NAME = "ABAP_AS_WITH_POOL"
  val SAP_JCO_ASHOST by ixtfEnvString("sap.jco.ashost", "appserver")
  val SAP_JCO_SYSNR by ixtfEnvString("sap.jco.sysnr", "00")
  val SAP_JCO_CLIENT by ixtfEnvString("sap.jco.client", "000")
  val SAP_JCO_USER by ixtfEnvString("sap.jco.user", "JCOTESTER")
  val SAP_JCO_PASSWD by ixtfEnvString("sap.jco.passwd", "JCOTESTERSPASSWORD")
  val SAP_JCO_LANG by ixtfEnvString("sap.jco.lang", "zh")
  @OptIn(FlowPreview::class)
  val SAP_JCO_POOL_CAPACITY by ixtfEnvInt("sap.jco.pool.capacity", DEFAULT_CONCURRENCY)

  val IXTF_API_JWT_KEY by ixtfEnvString("ixtf.api.jwt.key", "com.gitee.ixtf.sap")
}
