package com.gitee.ixtf.sap.application.internal

import com.gitee.ixtf.sap.application.SapService
import com.google.inject.Singleton
import com.sap.conn.jco.ext.DestinationDataProvider
import com.sap.conn.jco.ext.Environment
import jakarta.inject.Inject

@Singleton
class SapServiceImpl
@Inject
constructor(
    destinationDataProvider: DestinationDataProvider,
) : SapService {
  init {
    Environment.registerDestinationDataProvider(destinationDataProvider)
  }
}
