package com.gitee.ixtf.sap.application

import com.fasterxml.jackson.databind.JsonNode
import com.gitee.ixtf.sap.Constant.SAP_DESTINATION_NAME
import com.gitee.ixtf.sap.application.internal.SapServiceImpl
import com.gitee.ixtf.sap.kotlinx.fillData
import com.gitee.ixtf.sap.kotlinx.jsonMap
import com.google.inject.ImplementedBy
import com.sap.conn.jco.JCoDestinationManager

@ImplementedBy(SapServiceImpl::class)
interface SapService {
  suspend fun ping() = JCoDestinationManager.getDestination(SAP_DESTINATION_NAME).ping()

  suspend fun rfc(rfc: String, body: JsonNode): Map<String, Any?> =
      JCoDestinationManager.getDestination(SAP_DESTINATION_NAME).run {
        val function = repository.getFunction(rfc)
        function.fillData(body)
        function.execute(this)
        function.jsonMap()
      }
}
