package com.gitee.ixtf.sap.resource

import com.gitee.ixtf.sap.application.SapService
import com.gitee.ixtf.vertx.kotlinx.readJson
import com.google.inject.Singleton
import io.vertx.core.buffer.Buffer
import jakarta.annotation.security.PermitAll
import jakarta.inject.Inject
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam

@Singleton
@Path("")
class RestfulResource
@Inject
constructor(
    private val sapService: SapService,
) {
  @PermitAll @GET @Path("ping") suspend fun restful() = sapService.ping()

  @POST
  @Path("rfc/{rfc}")
  suspend fun restful(@PathParam("rfc") rfc: String, body: Buffer) =
      sapService.rfc(rfc, body.readJson())
}
