pluginManagement {
  repositories {
    mavenLocal()
    mavenCentral()
    gradlePluginPortal()
  }
}

dependencyResolutionManagement {
  repositories {
    mavenLocal()
    mavenCentral()
    maven("https://plugins.gradle.org/m2")
    maven("https://jitpack.io")
    maven("https://repo.akka.io/maven")
    maven("http://git.medipath.com.cn:8929/api/v4/projects/41/packages/maven") {
      isAllowInsecureProtocol = true
      credentials(HttpHeaderCredentials::class) {
        name = "Deploy-Token"
        value = "UqMuvxf7s7ykuL8SBev6"
      }
      authentication { create<HttpHeaderAuthentication>("header") }
    }
  }
}

rootProject.name = "sap"
